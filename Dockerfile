FROM ruby:2.5

RUN touch /var/log/access.log
WORKDIR /usr/src
ADD . /usr/src
RUN bin/setup
RUN bundle exec rake install
ENTRYPOINT ["s-log"]