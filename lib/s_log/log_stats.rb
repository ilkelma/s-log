require 'erb'
require 'pastel'
require 'tty-table'
require 'tty-pie_chart'

module SLog
  class LogStats
    attr_accessor :sections, :response_codes, :ips, :times
    attr_accessor :request_sizes, :idents, :users, :protocols
    attr_accessor :range, :lines, :bad_lines, :request_methods
    attr_reader :method_map


    def initialize
      @pastel = Pastel.new
      @bold_green = @pastel.bold.green.detach
      @red = @pastel.red.detach
      @underline = @pastel.underline.detach
      @sections = {}
      @total_requests = 0
      @request_methods = {}
      @response_codes = {}
      @times = {}
      @ips = {}
      @request_sizes = {}
      @idents = {}
      @users = {}
      @protocols = {}
      @range = (Time.now - 10)..Time.now
      @lines = []
      @bad_lines = []
      @template_str = <<~HEREDOC
        <%= @pastel.underline("Displaying stats from ") + @pastel.underline(@range.first) + @pastel.underline(" to ") + @pastel.underline(@range.last) %>
        <%= @bold_green["Total Good Requests:"] %> <%= @lines.size %> @ <%= '%.2f' % @lines.size.fdiv(10) %> RPS
        <% unless @bad_lines.empty? -%>
        <%= @red["Total Parsing Failures:"] %> <%= @bad_lines.size %>
        <% end -%>
        <%= @red["HTTP Error Responses"] %> <%= get_error_codes %>
        Unique users: <%= @users.size %>
        Unique IP Addresses: <%= @ips.size %>
        <% top_10_sections = top_10(@sections) -%>
        <% top_10_ips = top_10(@ips) -%>
        <% pie_chart_data = pie_chart_datafy(@request_methods) -%>
        <%= TTY::Table.new(['Top 10 Sections', 'Hits'], top_10_sections).render(:unicode, alignments: [:left, :right]) unless top_10_sections.empty? %>
        <%= TTY::Table.new(['Top 10 IP Addresses', 'Hits'], top_10_ips).render(:unicode, alignments: [:left, :right]) unless top_10_ips.empty? %>
        <%= TTY::PieChart.new(data: pie_chart_data) unless pie_chart_data.empty? %>
      HEREDOC
      @template = ERB.new(@template_str, 0, '-')
    end

    def total_requests
      @lines.size
    end

    def get_error_codes
      errs = 0
      @response_codes.each do |key, value|
        if (400..599).include? key.to_i
          errs += value
        end
      end
      return errs
    end

    def get_pie_chart_color(field)
      return case field
        when "HEAD" then :bright_cyan
        when "GET" then :bright_green
        when "POST" then :bright_red
        when "PUT" then :bright_blue
        when "PATCH" then :bright_yellow
        when "OPTIONS" then :bright_magenta
        when "-" then :bright_black
        when "DELETE" then :bright_white
        else Pastel::Color.new.style_names.select { |it| it =~ /^bright/ }.sample
      end
    end

    def pie_chart_datafy(field)
      ret_arr = []
      field.each do |key, value|
        ret_arr << {
          name: key,
          value: value,
          color: get_pie_chart_color(key)
        }
      end
      return ret_arr
    end

    def top_10(field)
      return [] unless field.kind_of? Hash
      field.sort_by(&:last).last(10).reverse
    end

    def purge
      index = @lines.find_index { |line| @range.include? line.time } 

      case index
      when 0
        # all times were in range, no purging necessary
        return
      when nil
        # No times were in range, purge all
        expired_lines = @lines
      else
        # If we found one, split lines on it
        index -= 1  
        expired_lines = @lines[0..index]
        @lines = @lines[index..-1]
      end

      expired_lines.each do |line|
        decrement(line)
      end
    end

    def increment(line)
      update(line, :inc)
    end

    def decrement(line)
      update(line, :dec)
    end

    def update(line, inc_or_dec)
      # This does some meta programming in order to set each
      # line instance variable automatically into the stats object
      # with the corresponding field
      # so ip is retrieved from the line and added to the ips hash
      line.instance_variables.each do |var|
        # Strip the symbol and @ from instance var
        var_plural = var.to_s.gsub(/:|@/, '') + "s"
        attribute_value = line.instance_variable_get(var)

        next unless self.class.method_defined? var_plural

        # If the value exists in the map, add to it
        if inc_or_dec == :inc
          if self.send(var_plural).key? attribute_value
            self.send(var_plural)[attribute_value] += 1
          else
            self.send(var_plural)[attribute_value] = 1
          end
        end

        if inc_or_dec == :dec
          if self.send(var_plural)[attribute_value] == 1
            self.send(var_plural).reject! {|key| key == attribute_value }
          elsif self.send(var_plural).key? attribute_value
            self.send(var_plural)[attribute_value] -= 1
          end
        end
      end
    end

    def render
      @template.result(binding)
    end

    def display
      puts render
    end
  end    
end