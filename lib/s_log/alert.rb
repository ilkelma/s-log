module SLog
  class Alert
    attr_accessor :window, :range, :state, :message, :entries, :threshold

    def initialize(window: 160, threshold: 10)
      @window = window.to_i
      @range = (Time.now - @window)..Time.now
      @state = :none
      @message = ""
      @entries = []
      @threshold = threshold.to_i
    end

    def check
      # First trim the entries
      index = @entries.find_index { |time| @range.include? time }

      unless index.nil?
        @entries = @entries[index..-1]
      end

      # do we need to alert?
      if @entries.size / @window > @threshold
        # if we've already set a message don't set one
        unless @state == :alerting
          @state = :alerting
          @message = Pastel.new.bold.red("High traffic generated an alert - hits = #{@entries.size}, triggered at #{Time.now}")
        end
      # We don't need to alert but previous state is alerted
      # So we should print a recovered message
      elsif @state == :alerting
        @state = :recovered
        @message = Pastel.new.bold.green("Alert recovered at #{Time.now}")
      end
    end
  end
end