require 's_log/log_stats'
require 's_log/log_line'
require 's_log/alert'
require 'time'

module SLog
  class LogAnalyze
    attr_reader :alert, :stats, :file

    def initialize(file, alert_threshold)
      @file = file
      @stats = SLog::LogStats.new
      @alert = SLog::Alert.new(threshold: alert_threshold)
    end

    def ingest(line)
      parsed_line = SLog::LogAnalyze.parse(line)
      # Verify the parsed line here so that we don't ingest bad data
      unless parsed_line.verify
        @stats.bad_lines.push(line)
        return
      end

      @stats.range = (Time.now - 10)..Time.now
      @alert.range = (Time.now - @alert.window)..Time.now

      if @stats.range.include? parsed_line.time
        @stats.increment(parsed_line)
        @stats.lines.push(parsed_line)
      end

      if @alert.range.include? parsed_line.time
        @alert.entries.push(parsed_line.time)
      end

      @stats.purge
      @alert.check
    end

    def reset_stats
      @stats = SLog::LogStats.new
    end

    def reset_alert
      @alert = SLog::Alert.new(threshold: @alert.threshold)
    end

    def reset
      reset_alert
      reset_stats
    end

    def self.parse(line)
      log_line = SLog::LogLine.new

      begin
        # first split on brackets
        bracket_split = line.rstrip.split(/\[|\]/)
        # then split on spaces
        left = bracket_split[0].split
        # we should also ditch the quotes
        right = bracket_split[2].gsub(/\"/, '').split

        log_line.ip, log_line.ident, log_line.user = left
        # time has lots of special cases we'll break it out for testability
        log_line.time = SLog::LogAnalyze.parse_time(bracket_split[1])
        log_line.request_method = right[0].upcase
        # section has lots of special cases we'll break it out for testability
        log_line.section = SLog::LogAnalyze.parse_section(right[1])
        log_line.protocol, log_line.response_code, log_line.request_size = right[2..4]
      rescue => e
        # We do nothing here because the verify call with flunk this line later
        # if there's bad data. Equally at present this naively ignores cause.
        puts e
        puts e.trace

      ensure
        return log_line
      end

      return log_line
    end

    def self.parse_time(time_string)
      return Time.strptime(time_string, "%d/%b/%Y:%H:%M:%S %z")
    end

    def self.parse_section(uri)
      return uri[/^\/(\w+)\//,1]
    end

    def display
      puts @alert.message unless @alert.state == :none
      @stats.display
    end
  end
end