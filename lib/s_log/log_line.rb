require 'ipaddr'
module SLog
  class LogLine
    attr_accessor :section, :total_request, :request_method, :response_code, :ip, :time, :request_size, :ident, :user, :protocol

    def initialize
      @section = ""
      @request_method = ""
      @response_code = ""
      @ip = ""
      @time = nil
      @request_size = ""
      @ident = ""
      @user = ""
      @protocol = ""
    end

    def verify
      return [
        verify_empty_null,
        verify_ip,
        verify_request_method,
        verify_response_code,
        verify_time,
        verify_request_size
      ].all?
    end

    def verify_empty_null
      self.instance_variables.none? do |var|
        [nil, ""].include? self.instance_variable_get(var)
      end
    end

    def verify_ip
      return true unless @ip == "-"
      begin
        IPAddr.new @ip
      rescue
        return false
      end
      return true
    end

    def verify_request_method
      %w(- GET POST PATCH PUT HEAD OPTIONS DELETE CONNECT TRACE).include? @request_method
    end

    def verify_response_code
      (100..599).include? @response_code.to_i
    end

    def verify_time
      @time.kind_of? Time
    end

    def verify_request_size
      @request_size =~ /^\d+$/
    end
  end    
end