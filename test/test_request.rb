class TestRequest
  attr_reader :good_request, :test_now, :good_now_request

  USERS = %w(fred janice frank steve milfred eureka robert james alison -)
  METHODS = %w(GET POST PUT HEAD PATCH OPTIONS DELETE -)
  # Words stolen from Schiller's Ode to Joy
  SECTIONS = %w(freude schoner gotterfunken tochter aus elysium)
  MIDS = %w(wir betreten feuertrunken himmlische dein heiligtum)
  ENDS = %w(deine zauber binden wieder was die mode streng geteilt)
  SUFFIX = %w(.jpg .png .html .htm)
  RESPONSE_CODES = (100..599).to_a
  PROTOCOLS = %w(HTTP/1.1 HTTP/1.0 HTTP/2 -)
  IP_BITS = (0..255).to_a

  def initialize
    @good_request = "84.122.226.11 - janice [15/Sep/2018:20:21:59 -0700] \"PUT /aus/dein/geteilt.htm HTTP/2\" 133 7285\n" 
    @test_now = Time.now
    @good_now_request = "84.122.226.11 - janice [#{@test_now.strftime("%d/%b/%Y:%H:%M:%S %z")}] \"PUT /aus/dein/geteilt.htm HTTP/2\" 133 7285\n" 
  end

  def self.create_phony_request
    ip = "#{IP_BITS.sample}.#{IP_BITS.sample}.#{IP_BITS.sample}.#{IP_BITS.sample}"
    return <<~REQUEST
      #{ip} - #{USERS.sample} [#{Time.now().strftime("%d/%b/%Y:%H:%M:%S %z")}] "#{METHODS.sample} /#{SECTIONS.sample}/#{MIDS.sample}/#{ENDS.sample}#{SUFFIX.sample} #{PROTOCOLS.sample}" #{RESPONSE_CODES.sample} #{rand(0..9999)}
    REQUEST
  end
end
