$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
$LOAD_PATH.unshift File.expand_path("../../test", __FILE__)
require "s_log/log_analyze"
require "s_log/version"
require "test_request"

require "minitest/autorun"
require "minitest/pride"
