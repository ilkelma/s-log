require 'minitest/autorun'
require 'minitest/pride'
require 'test_helper'
require 'open3'

class SLogTest < Minitest::Test

  def test_that_it_has_a_version_number
    refute_nil SLog::VERSION
  end

  def write_request(request: nil)
    @testfile.write request || TestRequest.create_phony_request
    @testfile.flush
  end

  def setup
    @testfile = File.new("test.log", "w")
    @slog = SLog::LogAnalyze.new(@testfile, 10)
    @request_helper = TestRequest.new
  end

  def test_phony
    assert_match(/-/, TestRequest.create_phony_request)
  end

  def test_write
    request = TestRequest.create_phony_request
    write_request(request: request)
    assert_equal(request, Open3.capture3("cat #{@testfile.path} | grep '#{request}'")[0])
  end

  def test_help
    stdout = Open3.capture3('./s-log --help', chdir: File.expand_path('exe'))[0]
    assert_match(/usage/i, stdout)
  end
  
  def test_parse_time
    correct_time = Time.new(2018,9,15,20,21,59,"-07:00") 
    assert_equal correct_time, SLog::LogAnalyze.parse_time("15/Sep/2018:20:21:59 -0700")
  end

  def test_parse_section
    correct_section = "aus"
    assert_equal correct_section, SLog::LogAnalyze.parse_section("/aus/dein/geteilt.htm")
  end

  def test_parse
    parsed_line = SLog::LogAnalyze.parse(TestRequest.create_phony_request)
    assert parsed_line.verify
  end

  def test_parse_with_foo_line
    parsed_line = SLog::LogAnalyze.parse("foo")
    refute parsed_line.verify
  end

  def test_alert
    @slog.reset
    @slog.alert.window = 5 

    150.times {
      @slog.ingest(TestRequest.create_phony_request)
    }

    assert_equal :alerting, @slog.alert.state
    assert_equal(55, @slog.alert.message.split[8].to_i)
  end

  def test_alert_and_recovery
    test_alert

    20.times {
      @slog.ingest(TestRequest.create_phony_request)
      sleep 0.5
    }

    assert_equal :recovered, @slog.alert.state
  end

  def test_total_request_counter
    @slog.reset

    150.times {
      @slog.ingest(TestRequest.create_phony_request)
    }

    assert_equal 150, @slog.stats.total_requests
  end

  def test_stats_ingestion
    @slog.reset
    @slog.ingest(@request_helper.good_now_request)
    assert_equal(1, @slog.stats.total_requests)
    assert_equal(1, @slog.stats.ips["84.122.226.11"])
    assert_equal(1, @slog.stats.idents["-"])
    assert_equal(1, @slog.stats.users["janice"])
    assert_equal(1, @slog.stats.request_methods["PUT"])
    assert_equal(1, @slog.stats.sections["aus"])
    assert_equal(1, @slog.stats.protocols["HTTP/2"])
    assert_equal(1, @slog.stats.response_codes["133"])
    assert_equal(1, @slog.stats.request_sizes["7285"])
    # assert_includes(@slog.stats.times, @request_helper.test_now)
  end

  def teardown
    @testfile.close
    File.delete(@testfile)
  end

end